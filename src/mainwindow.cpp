#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>

#include "qt-crc32/crc32.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_calculate_clicked()
{
    if (ui->filePath->text().isEmpty())
    {
        ui->crc->setText("Empty input");
        return;
    }

    if (!QFile(ui->filePath->text()).exists())
    {
        ui->crc->setText("No such file");
        return;
    }

    if (QFile(ui->filePath->text()).size() == 0)
    {
        ui->crc->setText("Empty or invalid file");
        return;
    }

    Crc32 crc;
    ui->crc->setText(QString::number( crc.calculateFromFile(ui->filePath->text()), 16 ));
}

void MainWindow::on_searchForFile_clicked()
{
    if (lastPath.isEmpty())
        lastPath = QDir::homePath();

    QFileDialog dialog(this, "Select file", lastPath);
    if (dialog.exec())
    {
        lastPath = QFileInfo(dialog.selectedFiles().first()).path();
        ui->filePath->setText(dialog.selectedFiles().first());
    }
}

void MainWindow::on_filePath_textChanged(const QString)
{
    ui->crc->clear();
}
