#include "mainwindow.h"
#include <QApplication>
#include <QCommandLineParser>
#include <QDebug>

#include "qt-crc32/crc32.h"

static QTextStream& qStdOut()
{
    static QTextStream ts( stdout );
    return ts;
}

int main(int argc, char *argv[])
{
    QCoreApplication::setApplicationVersion("1.0");
    QCoreApplication::setApplicationName("CRC Calculator");
    QCoreApplication::setOrganizationName("Volk_Milit");

    QApplication a(argc, argv);

    QCommandLineParser parser;
    parser.setApplicationDescription("CRC Calculator (c) 2019 Volk_Milit (aka Ja'Virr-Dar)");
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addOption(QCommandLineOption({"f", "file"}, "Set file calculate CRC from.", "file path"));
    parser.process(a);

    if (parser.isSet("file") || parser.isSet("f"))
    {
        QString file = parser.value("file");

        if (QFile(file).exists())
        {
            Crc32 crc;
            qStdOut() << QString::number( crc.calculateFromFile(file), 16 ) << "\n";

            return 0;
        }
        else
        {
            return 1;
        }
    }

    MainWindow w;
    w.show();

    return a.exec();
}
