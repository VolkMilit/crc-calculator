#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

    private slots:
        void on_calculate_clicked();
        void on_searchForFile_clicked();
        void on_filePath_textChanged(const QString);

    private:
        Ui::MainWindow *ui;
        QString lastPath;
};

#endif // MAINWINDOW_H
